﻿using System;
using AgentOctal.WpfLib.Services.Message;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WpfLib.UnitTests
{
    [TestClass]
    public class MessengerTests
    {
        [TestMethod]
        public void MessageSubscribe()
        {
            var messenger = new MessageService();
            bool recieaved = false;

            messenger.Subscribe<TestMessage>(message => recieaved = true);
            messenger.SendMessage(new TestMessage());


            Assert.IsTrue(recieaved);
        }

        [TestMethod]
        public void MessageTypeMatches()
        {
            var messenger = new MessageService();
            bool recieaved = false;

            messenger.Subscribe<TestMessage>(message => recieaved = true);
            messenger.SendMessage(new TestMessage2());


            Assert.IsFalse(recieaved);
        }

        [TestMethod]
        public void MessageUnsubscribe()
        {
            var messenger = new MessageService();
            bool recieaved = false;

            Action<IMessage> handler = message => recieaved = true;

            messenger.Subscribe<TestMessage>(handler);
            messenger.Unsubscribe<TestMessage>(handler);
            messenger.SendMessage(new TestMessage());

            Assert.IsFalse(recieaved);
        }

        [TestMethod]
        public void MessageUnsubscribeDifferentEvent()
        {
            var messenger = new MessageService();
            bool recieaved1 = false;
            bool recieaved2 = false;

            Action<IMessage> handler1 = message => recieaved1 = true;
            Action<IMessage> handler2 = message => recieaved2 = true;

            messenger.Subscribe<TestMessage>(handler1);
            messenger.Subscribe<TestMessage2>(handler2);
            messenger.Unsubscribe<TestMessage>(handler1);
            messenger.SendMessage(new TestMessage());
            messenger.SendMessage(new TestMessage2());

            Assert.IsFalse(recieaved1);
            Assert.IsTrue(recieaved2);
        }

        [TestMethod]
        public void MessageReceived()
        {
            var messenger = new MessageService();
            bool recieaved = false;

            messenger.Subscribe<TestMessage>(message => recieaved = true);
            messenger.SendMessage(new TestMessage());


            Assert.IsTrue(recieaved);
        }

        [TestMethod]
        public void TestGarbageDisposal()
        {
            var messenger = new MessageService();

            WeakReference reference = null;
            new Action(() =>
            {
                var receiver = new TestReceiver(messenger, (message) => Assert.Fail("Message was received"));
                reference = new WeakReference(receiver, true);
                Assert.IsNotNull(reference.Target);
            })();

            GC.Collect();
            GC.WaitForPendingFinalizers();

            messenger.SendMessage(new TestMessage());

            Assert.IsNull(reference.Target);
        }

        private class TestReceiver
        {
            Action<IMessage> _handlerBody = null;

            public TestReceiver(MessageService messenger, Action<IMessage> handlerBody)
            {
                _handlerBody = handlerBody;
                messenger.Subscribe<TestMessage>(_handlerBody);
            }
        }

        private class TestMessage : IMessage
        {

        }
        private class TestMessage2 : IMessage
        {

        }
    }
}
