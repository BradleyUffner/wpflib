﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using AgentOctal.WpfLib;

namespace WpfLib.Tests.VmCallMarkupExtension
{
    class DemoWindowVm:ViewModel
    {
        public void Click(EventArgs e)
        {
            MessageBox.Show("Click!");
        }

        public void Preview()
        {
            MessageBox.Show("PreviewDown!");
        }
    }
}
