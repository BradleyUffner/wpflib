﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using AgentOctal.WpfLib.Services;
using AgentOctal.WpfLib.Services.WindowManager;

namespace WpfLib.Tests
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            ServiceManager.GetService<IWindowManagerService>()
                .DisplayWindowFor(new IndexWindowVm());
        }
    }
}
