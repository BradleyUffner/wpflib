﻿using System.Windows.Input;

namespace WpfLib.Tests
{
    internal class DemoInfo
    {
        public string Name { get; set; }
        public ICommand Command { get; set; }
    }
}