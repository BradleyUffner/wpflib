﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using AgentOctal.WpfLib;
using AgentOctal.WpfLib.Commands;

namespace WpfLib.Tests
{
    class CancelWindowCloseVm : ViewModel,IWindowClosingQueryable
    {

        private bool _allowClose;

        public bool AllowClose
        {
            get { return _allowClose; }
            set { SetValue(ref _allowClose, value); }
        }

        private ICommand _windowCloseCommand;
        public ICommand WindowCloseCommand
        {
            get
            {
                return _windowCloseCommand ?? (_windowCloseCommand = new SimpleCommand((arg) =>
                       {
                           var closeArgs = arg as CancelEventArgs;
                           closeArgs.Cancel = !AllowClose;
                       }));
            }
        }

        public void WindowClosing(CancelEventArgs e)
        {
            e.Cancel = !AllowClose;
        }
    }
}
