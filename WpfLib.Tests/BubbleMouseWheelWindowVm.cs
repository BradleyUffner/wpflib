﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentOctal.WpfLib;

namespace WpfLib.Tests
{
    class BubbleMouseWheelWindowVm : ViewModel
    {
        public string Text => @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam faucibus ornare magna id tempus. Aliquam erat volutpat. Ut libero justo, vulputate ac laoreet commodo, fermentum vestibulum nisi. Nunc placerat felis non mi vestibulum, ac vestibulum lorem blandit. Nunc finibus vehicula felis, eget dictum neque sagittis vel. Suspendisse non nisi est. Sed placerat lobortis tellus. Quisque eu velit sit amet ex venenatis facilisis. Proin pretium ex urna, nec malesuada orci consectetur quis. Suspendisse pharetra venenatis sagittis. Sed lacinia sapien diam, nec ultrices tellus efficitur non. Praesent vitae venenatis mi. Proin non aliquam sapien. Nulla elit lectus, maximus nec quam non, faucibus dictum urna. Mauris ac ultricies nibh.

Morbi quis pulvinar leo. Vestibulum scelerisque quis leo scelerisque consequat. Nam euismod, nibh ac gravida lobortis, tortor leo aliquam diam, vel auctor enim erat in dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id ex mollis, iaculis felis ac, viverra lorem. Maecenas mattis magna risus, quis molestie erat venenatis vel. Proin a turpis sit amet lorem faucibus malesuada eu at sapien.";
    }
}
