﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using AgentOctal.WpfLib;
using AgentOctal.WpfLib.Commands;

namespace WpfLib.Tests
{
    class AutoScrollingItemsViewTestVm : ViewModel
    {
        private DispatcherTimer _timer;

        public AutoScrollingItemsViewTestVm()
        {
            Items = new ObservableCollection<string>();
            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(.25);
            _timer.Tick += (o, e) => AddItemCommand.Execute(null);
            _timer.IsEnabled = true;
        }

        public ObservableCollection<string> Items { get; }

        private ICommand _addItemCommand;
        public ICommand AddItemCommand
        {
            get
            {
                return _addItemCommand ?? (_addItemCommand = new SimpleCommand((obj) =>
                {
                    Items.Add($"Clicked at {DateTime.Now.ToString()}");
                }));
            }
        }

        private ICommand _clearItemsCommand;
        public ICommand ClearItemsCommand
        {
            get
            {
                return _clearItemsCommand ?? (_clearItemsCommand = new SimpleCommand((obj) =>
                {
                    Items.Clear();
                }));
            }
        }
    }
}
