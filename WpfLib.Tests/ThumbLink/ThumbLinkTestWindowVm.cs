﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentOctal.WpfLib;

namespace WpfLib.Tests.ThumbLink
{
    class ThumbLinkTestWindowVm : ViewModel
    {
        public ThumbLinkTestWindowVm()
        {
            Things = new ObservableCollection<Thing>();

            Things.Add(new Thing() { Width = 50, Height = 50 });
        }

        public ObservableCollection<Thing> Things { get; }

    }

    class Thing : ViewModel
    {
        private double _x;
        public double X
        {
            get { return _x; }
            set { SetValue(ref _x, value); }
        }

        private double _y;
        public double Y
        {
            get { return _y; }
            set { SetValue(ref _y, value); }
        }

        private double _width;
        public double Width
        {
            get { return _width; }
            set { SetValue(ref _width, value); }
        }

        private double _height;
        public double Height
        {
            get { return _height; }
            set { SetValue(ref _height, value); }
        }


        private AgentOctal.WpfLib.Behaviors.ThumbLink.ThumbDeltaInfo _positionDelta;
        public AgentOctal.WpfLib.Behaviors.ThumbLink.ThumbDeltaInfo PositionDelta
        {
            get { return new AgentOctal.WpfLib.Behaviors.ThumbLink.ThumbDeltaInfo(); }
            set
            {
                if (SetValue(ref _positionDelta, value))
                {
                    X += value.HorizontalChange;
                    Y += value.VerticalChange;
                }
            }
        }

        private AgentOctal.WpfLib.Behaviors.ThumbLink.ThumbDeltaInfo _sizeDelta;
        public AgentOctal.WpfLib.Behaviors.ThumbLink.ThumbDeltaInfo SizeDelta
        {
            get { return new AgentOctal.WpfLib.Behaviors.ThumbLink.ThumbDeltaInfo(); }
            set
            {
                if (SetValue(ref _sizeDelta, value))
                {
                    Width += value.HorizontalChange;
                    Height += value.VerticalChange;
                }
            }
        }


    }
}
