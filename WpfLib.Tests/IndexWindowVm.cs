﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using AgentOctal.WpfLib;
using AgentOctal.WpfLib.Commands;
using WpfLib.Tests.MvvmRoutedCommands;

namespace WpfLib.Tests
{
    class IndexWindowVm : ViewModel
    {
        public IndexWindowVm()
        {

            var testList = new ObservableCollection<int>();
            testList.AddRange(Enumerable.Range(1,4));
            testList.AddRange(new List<int>( new[] { 5, 6, 7, 8, 9 }));


            _demoList = new List<DemoInfo>();

            _demoList.Add(new DemoInfo()
            {
                Name = "Cancel Window Close",
                Command = DemoCloseBehaviorCommand
            });

            _demoList.Add(new DemoInfo()
            {
                Name = "BubbleMouseWheelBehavior",
                Command = DemoBubbleMouseWheelBehaviorCommand
            });

            _demoList.Add(new DemoInfo()
            {
                Name = "Cascading property change",
                Command = PropertyChangeCascadeCommand
            });

            _demoList.Add(new DemoInfo()
            {
                Name = "MvvmRoutedCommand test",
                Command = RoutedCommandTestCommand
            });

            _demoList.Add(new DemoInfo()
            {
                Name = "Auto scrolling ItemsControl",
                Command = AutoscrollingItemsControlTextCommand
            });

            _demoList.Add(new DemoInfo()
                          {
                              Name = "VmCall",
                              Command = VmCallCommand
                          });

            _demoList.Add(new DemoInfo()
                          {
                              Name = "Thumb",
                              Command = ThumbCommand
                          });
        }

        List<DemoInfo> _demoList;

        public List<DemoInfo> DemoList
        {
            get { return _demoList; }
        }


        private ICommand _commandNameCommand;

        public ICommand DemoCloseBehaviorCommand
        {
            get
            {
                return _commandNameCommand ?? (_commandNameCommand = new AgentOctal.WpfLib.Commands.SimpleCommand((obj) =>
                {
                    var vm = new CancelWindowCloseVm();
                    WindowManager.DisplayWindowFor(vm);
                }));
            }
        }

        private ICommand _demoCommand;

        public ICommand DemoBubbleMouseWheelBehaviorCommand
        {
            get
            {
                return _demoCommand ?? (_demoCommand = new AgentOctal.WpfLib.Commands.SimpleCommand((obj) =>
                {
                    var vm = new BubbleMouseWheelWindowVm();
                    WindowManager.DisplayWindowFor(vm);
                }));
            }
        }

        private ICommand _propertyChangeCascadeCommand;

        public ICommand PropertyChangeCascadeCommand
        {
            get
            {
                return _propertyChangeCascadeCommand ?? (_propertyChangeCascadeCommand = new SimpleCommand((obj) =>
                {
                    var test = new CascadingPropertyVM();
                    test.Name = "Bradley";
                }));
            }
        }

        private ICommand _routedCommandTestCommand;

        public ICommand RoutedCommandTestCommand
        {
            get
            {
                return _routedCommandTestCommand ?? (_routedCommandTestCommand = new SimpleCommand((obj) =>
                {
                    var vm = new RoutedCommandTestWindowVm();
                    WindowManager.DisplayWindowFor(vm);
                }));
            }
        }

        private ICommand _autoscrollingItemsControlTextCommand;

        public ICommand AutoscrollingItemsControlTextCommand
        {
            get
            {
                return _autoscrollingItemsControlTextCommand ?? (_autoscrollingItemsControlTextCommand = new SimpleCommand((obj) =>
                {
                    var vm = new AutoScrollingItemsViewTestVm();
                    WindowManager.DisplayWindowFor(vm);
                }));
            }
        }


        private ICommand _vmCallCommand;

        public ICommand VmCallCommand
        {
            get
            {
                return _vmCallCommand ?? (_vmCallCommand = new SimpleCommand((obj) =>
                {
                    var vm = new VmCallMarkupExtension.DemoWindowVm();
                    WindowManager.DisplayWindowFor(vm);
                }));
            }
        }


        private ICommand _thumbCommand;

        public ICommand ThumbCommand
        {
            get
            {
                return _thumbCommand ?? (_thumbCommand = new SimpleCommand((obj) =>
                {
                    var vm = new ThumbLink.ThumbLinkTestWindowVm();
                    WindowManager.DisplayWindowFor(vm);
                }));
            }
        }
    }
}