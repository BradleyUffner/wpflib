﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using AgentOctal.WpfLib;
using AgentOctal.WpfLib.Commands;

namespace WpfLib.Tests.MvvmRoutedCommands
{
    class RoutedCommandTestWindowVm : ViewModel
    {
        private ICommand _testCommandCommand;
        public ICommand TestCommandCommand
        {
            get
            {
                return _testCommandCommand ?? (_testCommandCommand = new SimpleCommand((obj) =>
                {
                    WindowManager.ShowMessage("It Works!");
                }));
            }
        }
    }
}
