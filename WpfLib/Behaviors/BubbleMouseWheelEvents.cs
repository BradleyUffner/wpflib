﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Resources;
using GumpStudio.WpfLib;

namespace AgentOctal.WpfLib.Behaviors
{
    /// <summary>
    /// Causes a Control to bubble MouseWheel events up to the parent if an internal ScrollViewer is at its minimium or maximium position.
    /// </summary>
    /// <remarks>
    /// Inspired by https://stackoverflow.com/questions/1585462/bubbling-scroll-events-from-a-listview-to-its-parent
    /// </remarks>
    public class BubbleMouseWheelEvents : AttachableForStyleBehavior<UIElement, BubbleMouseWheelEvents>
    {
        protected override void OnAttached()
        {
            AssociatedObject.PreviewMouseWheel += PreviewMouseWheel;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseWheel -= PreviewMouseWheel;
        }

        private void PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var scrollViewer = AssociatedObject.GetChildOf<ScrollViewer>(includeSelf: true);
            var scrollPos = scrollViewer.ContentVerticalOffset;
            if ((scrollPos == scrollViewer.ScrollableHeight && e.Delta < 0) || (scrollPos == 0 && e.Delta > 0))
            {
                UIElement rerouteTo = AssociatedObject;
                if (ReferenceEquals(scrollViewer, AssociatedObject))
                {
                    rerouteTo = (UIElement)VisualTreeHelper.GetParent(AssociatedObject);
                }

                e.Handled = true;
                var e2 = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                e2.RoutedEvent = UIElement.MouseWheelEvent;
                rerouteTo.RaiseEvent(e2);
            }
        }
    }
}
