﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace AgentOctal.WpfLib.Behaviors
{

    /// <summary>
    /// A behavior that causes TextBoxes to automatically select their content when focused.
    /// </summary>
    public class AutoSelectTextBox : Behavior<TextBox>
    {

        protected override void OnAttached()
        {
            AssociatedObject.GotFocus += OnFocus;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.GotFocus -= OnFocus;
        }

        private void OnFocus(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke((Action)delegate ()
            {
                AssociatedObject?.SelectAll();
            });
        }
    }
}
