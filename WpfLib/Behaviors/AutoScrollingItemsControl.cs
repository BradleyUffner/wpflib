﻿using System;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using GumpStudio.WpfLib;

namespace AgentOctal.WpfLib.Behaviors
{

    [Obsolete("Use AutoScrollBehavior")]
    public class AutoScrollingItemsControl : Behavior<ItemsControl>
    {
        private ScrollViewer _scrollViewer;
        private bool _autoScroll = true;
        private bool _justWheeled = false;
        private bool _userInteracting = false;


        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.Loaded += AssociatedObject_OnLoaded;
            AssociatedObject.Unloaded += AssociatedObject_OnUnloaded;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.Loaded -= AssociatedObject_OnLoaded;
            AssociatedObject.Unloaded -= AssociatedObject_OnUnloaded;
        }

        private void AssociatedObject_OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            _scrollViewer = AssociatedObject.FindParentOf<ScrollViewer>();
            if (_scrollViewer != null)
            {
                _scrollViewer.ScrollChanged += ScrollViewer_OnScrollChanged;
                AssociatedObject.ItemContainerGenerator.ItemsChanged += ItemContainerGenerator_ItemsChanged;
                AssociatedObject.GotMouseCapture += AssociatedObject_GotMouseCapture;
                AssociatedObject.LostMouseCapture += AssociatedObject_LostMouseCapture;
                AssociatedObject.PreviewMouseWheel += AssociatedObject_PreviewMouseWheel;
            }
        }

        private void AssociatedObject_OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (_scrollViewer != null)
            {
                _scrollViewer.ScrollChanged -= ScrollViewer_OnScrollChanged;
                AssociatedObject.ItemContainerGenerator.ItemsChanged -= ItemContainerGenerator_ItemsChanged;
                AssociatedObject.GotMouseCapture -= AssociatedObject_GotMouseCapture;
                AssociatedObject.LostMouseCapture -= AssociatedObject_LostMouseCapture;
                AssociatedObject.PreviewMouseWheel -= AssociatedObject_PreviewMouseWheel;
            }

            _scrollViewer = null;
        }

        void AssociatedObject_GotMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            // User is actively interacting with the control. Do not allow automatic scrolling to interfere with user experience.
            _userInteracting = true;
            _autoScroll = false;
        }

        void AssociatedObject_LostMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            // User is done interacting with control.
            _userInteracting = false;
        }

        private void ScrollViewer_OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            // diff is exactly zero if the last item in the list is visible. This can occur because of scroll-bar drag, mouse-wheel, or keyboard event.
            double diff = (_scrollViewer.VerticalOffset - (_scrollViewer.ExtentHeight - _scrollViewer.ViewportHeight));

            // User just wheeled; this event is called immediately afterwards.
            if (_justWheeled && diff != 0.0)
            {
                _justWheeled = false;
                _autoScroll = false;
                return;
            }

            if (diff == 0.0)
            {
                // then assume user has finished with interaction and has indicated through this action that scrolling should continue automatically.
                _autoScroll = true;
            }
        }

        private void ItemContainerGenerator_ItemsChanged(object sender, System.Windows.Controls.Primitives.ItemsChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Reset)
            {
                // An item was added to the listbox, or listbox was cleared.
                if (_autoScroll && !_userInteracting)
                {
                    // If automatic scrolling is turned on, scroll to the bottom to bring new item into view.
                    // Do not do this if the user is actively interacting with the listbox.
                    _scrollViewer.ScrollToBottom();
                }
            }
        }

        void AssociatedObject_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            // User wheeled the mouse. 
            // Cannot detect whether scroll viewer right at the bottom, because the scroll event has not occurred at this point.
            // Same for bubbling event.
            // Just indicated that the user mouse-wheeled, and that the scroll viewer should decide whether or not to stop autoscrolling.
            _justWheeled = true;
        }
    }
}
