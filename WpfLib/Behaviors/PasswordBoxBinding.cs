﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace AgentOctal.WpfLib.Behaviors
{
    /// <summary>
    /// Allows binding of a PasswordBox's Password property to a viewmodel.
    /// </summary>
    public class PasswordBoxBinding : Behavior<PasswordBox>
    {
        protected override void OnAttached()
        {
            AssociatedObject.PasswordChanged += PasswordBox_PasswordChanged;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PasswordChanged -= PasswordBox_PasswordChanged;
        }

        public SecureString Password
        {
            get { return (SecureString)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password",
                                        typeof(SecureString),
                                        typeof(PasswordBoxBinding),
                                        new PropertyMetadata(null));

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var binding = BindingOperations.GetBindingExpression(this, PasswordProperty);
            if (binding == null) return;

            PropertyInfo property = binding.DataItem.GetType()
                                           .GetProperty(binding.ParentBinding.Path.Path);

            if (property != null)
            {
                property.SetValue(binding.DataItem, AssociatedObject.SecurePassword, null);
            }
        }

    }
}
