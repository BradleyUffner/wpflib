﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace AgentOctal.WpfLib.Behaviors
{
    public class OnClosingBehavior : Behavior<Window>
    {

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(OnClosingBehavior));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.Closing += WindowClosing;
        }

        protected override void OnDetaching()
        {
            this.AssociatedObject.Closing -= WindowClosing;
            base.OnDetaching();
        }

        [SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
        private void WindowClosing(object sender, CancelEventArgs e)
        {
            ICommand command = GetValue(CommandProperty) as ICommand;
            if (command != null)
            {
                var args = new CancelEventArgs();
                command.Execute(args);
                e.Cancel = args.Cancel;
            }
        }
    }
}
