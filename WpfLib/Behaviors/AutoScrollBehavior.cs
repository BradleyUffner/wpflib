﻿using System;
using System.Windows;
using System.Windows.Controls;
using GumpStudio.WpfLib;

namespace AgentOctal.WpfLib.Behaviors
{
    public class AutoScrollBehavior : AttachableForStyleBehavior<ItemsControl, AutoScrollBehavior>
    {
        private ScrollViewer _scrollViewer;

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.Loaded += AssociatedObject_OnLoaded;
            AssociatedObject.Unloaded += AssociatedObject_OnUnloaded;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.Loaded -= AssociatedObject_OnLoaded;
            AssociatedObject.Unloaded -= AssociatedObject_OnUnloaded;
        }

        private void AssociatedObject_OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            _scrollViewer = AssociatedObject.FindParentOf<ScrollViewer>();
            if (_scrollViewer != null)
            {
                _scrollViewer.ScrollChanged += ScrollViewer_OnScrollChanged;
            }
        }

        private void AssociatedObject_OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (_scrollViewer != null)
            {
                _scrollViewer.ScrollChanged -= ScrollViewer_OnScrollChanged;
            }
            _scrollViewer = null;
        }

        private bool _autoScroll = true;

        private void ScrollViewer_OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            // User scroll event : set or unset auto-scroll mode
            if (e.ExtentHeightChange == 0)
            {   // Content unchanged : user scroll event
                if (_scrollViewer.VerticalOffset == _scrollViewer.ScrollableHeight)
                {   // Scroll bar is at bottom
                    // Set auto-scroll mode
                    _autoScroll = true;
                }
                else
                {   // Scroll bar isn't at bottom
                    // Unset auto-scroll mode
                    _autoScroll = false;
                }
            }

            // Content scroll event : auto-scroll eventually
            if (_autoScroll && e.ExtentHeightChange != 0)
            {   // Content changed and auto-scroll mode set
                // Autoscroll
                _scrollViewer.ScrollToVerticalOffset(_scrollViewer.ExtentHeight);
            }
        }
    }
}
