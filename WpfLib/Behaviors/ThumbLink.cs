﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;

namespace AgentOctal.WpfLib.Behaviors
{
    public class ThumbLink : Behavior<Thumb>
    {
        protected override void OnAttached()
        {
            AssociatedObject.DragDelta += AssociatedObject_DragDelta;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.DragDelta -= AssociatedObject_DragDelta;
        }

        private void AssociatedObject_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var delta = new ThumbDeltaInfo(e.HorizontalChange, e.VerticalChange);
            ThumbDelta = delta;
            e.Handled = true;
        }

        public ThumbDeltaInfo ThumbDelta
        {
            get { return (ThumbDeltaInfo)GetValue(ThumbDeltaProperty); }
            set { SetValue(ThumbDeltaProperty, value); }
        }

        public static readonly DependencyProperty ThumbDeltaProperty =
            DependencyProperty.Register("ThumbDelta", typeof(ThumbDeltaInfo), typeof(ThumbLink),
                                        new FrameworkPropertyMetadata(defaultValue: default(ThumbDeltaInfo),
                                                                      flags: FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public struct ThumbDeltaInfo
        {
            public ThumbDeltaInfo(double horizontalChange, double verticalChange)
            {
                HorizontalChange = horizontalChange;
                VerticalChange = verticalChange;
            }

            public double HorizontalChange;
            public double VerticalChange;

        }
    }
}
