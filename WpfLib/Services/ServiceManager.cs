﻿using System;
using System.Collections.Generic;
using AgentOctal.WpfLib.Services.Message;
using AgentOctal.WpfLib.Services.WindowManager;

namespace AgentOctal.WpfLib.Services
{

    /// <summary>
    /// A simple service locator that allows developers to register and retreive service instances by their interface type.
    /// </summary>
    public static class ServiceManager
    {
        private static readonly Dictionary<Type, object> Services;

        static ServiceManager()
        {
            Services = new Dictionary<Type, object>();

            RegisterService<IMessageService>(new MessageService());
            RegisterService<IWindowManagerService>(new WindowManagerService());
        }

        /// <summary>
        /// Gets a prevoously registered service by an interface.
        /// </summary>
        /// <typeparam name="T">The Interface of the type of service to return.</typeparam>
        /// <returns>The prevously registered instance that implements the specified interface Type.</returns>
        public static T GetService<T>()
        {
            return (T)Services[typeof(T)];
        }

        /// <summary>
        /// Registers a service implementation for an interface.
        /// </summary>
        /// <typeparam name="T">The Type of the Interface the service implements.</typeparam>
        /// <param name="serviceInstance">An instance of the service to register.</param>
        /// <returns>Returns the registered service instance.</returns>
        public static T RegisterService<T>(T serviceInstance)
        {
            Services[typeof(T)] = serviceInstance;
            return serviceInstance;
        }

        /// <summary>
        /// Returns the currently registered IWindowManagerService
        /// </summary>
        public static IWindowManagerService WindowManagerService => GetService<IWindowManagerService>();

        /// <summary>
        /// Returns the currently registered IMessengerService
        /// </summary>
        public static IMessageService MessageService => GetService<IMessageService>();
    }
}
