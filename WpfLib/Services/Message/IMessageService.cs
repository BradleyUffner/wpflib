﻿using System;

namespace AgentOctal.WpfLib.Services.Message
{
    public interface IMessageService
    {
        void SendMessage(IMessage message);
        void Subscribe<T>(Action<IMessage> handler) where T : IMessage;
        void Unsubscribe<T>(Action<IMessage> handler) where T : IMessage;
    }
}
