﻿using System;
using System.Collections.Generic;

namespace AgentOctal.WpfLib.Services.Message
{
    public class MessageService : IMessageService
    {
        private readonly Dictionary<Type, List<WeakReference<Action<IMessage>>>> _handlersFor;

        public MessageService()
        {
            _handlersFor = new Dictionary<Type, List<WeakReference<Action<IMessage>>>>();
        }

        public void SendMessage(IMessage message)
        {
            if (_handlersFor.TryGetValue(message.GetType(), out var handlers))
            {
                foreach (WeakReference<Action<IMessage>> handlerReference in handlers)
                {
                    if (handlerReference.TryGetTarget(out var handler))
                    {
                        handler.Invoke(message);
                    }
                }
            }
        }

        public void Subscribe<T>(Action<IMessage> handler) where T : IMessage
        {
            if (!_handlersFor.TryGetValue(typeof(T), out var handlers))
            {
                handlers = new List<WeakReference<Action<IMessage>>>();
                _handlersFor.Add(typeof(T), handlers);
            }
            handlers.Add(new WeakReference<Action<IMessage>>(handler));
        }

        public void Unsubscribe<T>(Action<IMessage> handler) where T : IMessage
        {
            if (_handlersFor.TryGetValue(typeof(T), out var handlers))
            {
                handlers.RemoveAll(handlerReference =>
                {
                    if (handlerReference.TryGetTarget(out var h))
                    {
                        return h == handler;
                    }
                    return false;
                });
            }
        }
    }
}
