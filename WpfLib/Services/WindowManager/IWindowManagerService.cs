﻿using System;
using System.Windows;
using AgentOctal.WpfLib.Services.WindowManager.WindowInitializers;

namespace AgentOctal.WpfLib.Services.WindowManager
{
    public interface IWindowManagerService
    {
        /// <summary>
        /// Registers the default view to display for a particular view model type.
        /// </summary>
        /// <typeparam name="TViewModelType">The type of the view model.</typeparam>
        /// <typeparam name="TWindowType">The type of the view (window) to display.</typeparam>
        /// <returns>Returns the same WindowManagerService instance for chaining.</returns>
        IWindowManagerService RegisterDefaultWindow<TViewModelType, TWindowType>();

        /// <summary>
        /// Creates a window of the associated type, binds it to the view model, and displays it.
        /// </summary>
        /// <param name="viewModel">The view model to bind.</param>
        /// <returns></returns>
        Window DisplayWindowFor(ViewModel viewModel);

        /// <summary>
        /// Creates a window of the specified type, binds it to the view model, and displays it.
        /// </summary>
        /// <param name="viewModel">The view model to bind.</param>
        /// <param name="windowType">The type of view to create and bind.</param>
        /// <returns></returns>
        Window DisplayWindowFor(ViewModel viewModel, Type windowType);

        /// <summary>
        /// Creates a window of the associated type as a child of the window associated 
        /// to the specified parent view model, binds it to the view model, and displays it.
        /// </summary>
        /// <param name="childViewModel">The view model to create a window for.</param>
        /// <param name="parentViewModel">The view model to attach the new window to as a child.</param>
        /// <returns></returns>
        Window DisplayChildWindowFor(ViewModel childViewModel, ViewModel parentViewModel);

        /// <summary>
        /// Creates a window of the associated type as a child of the last created window,
        /// binds it to the view model, and displays it.
        /// </summary>
        /// <param name="childViewModel"></param>
        /// <returns></returns>
        Window DisplayChildWindowFor(ViewModel childViewModel);

        /// <summary>
        /// Creates a new window for the specified view model and displays it as a dialog.
        /// </summary>
        /// <param name="viewModel">The view model to display a dialog for.</param>
        /// <returns>Returns the dialog result.</returns>
        bool DisplayDialogFor(ViewModel viewModel);

        /// <summary>
        /// Closes the window associated to the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model associated with the window to close.</param>
        void Close(ViewModel viewModel);

        /// <summary>
        /// Sets the dialog result of the dialog associated with the specified view model and closes it.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="dialogResult"></param>
        void SetDialogResult(ViewModel viewModel, bool dialogResult);

        /// <summary>
        /// Closes all the windows managed by the WindowManager.
        /// </summary>
        void CloseAll();

        /// <summary>
        /// Displays a standard message box.
        /// </summary>
        /// <param name="message">The text to display in the mesage box.</param>
        void ShowMessage(string message);

        /// <summary>
        /// The WindowInitializer instance that will be called on newly created windows.
        /// </summary>
        IWindowInitializer WindowInitializer{get;set;}
    }
}
