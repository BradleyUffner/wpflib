﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using AgentOctal.WpfLib.Services.WindowManager.WindowInitializers;
using AgentOctal.WpfLib.Services.WindowManager.WindowTypeResolvers;

namespace AgentOctal.WpfLib.Services.WindowManager
{
    public class WindowManagerService : IWindowManagerService
    {

        public static IWindowManagerService RegisterAsDefault()
        {
            return ServiceManager.RegisterService<IWindowManagerService>(new WindowManagerService());
        }

        private readonly Dictionary<Type, Type> _windowMap; //Maps a ViewModel Type to its default Window Type
        private readonly List<Window> _openWindows; //a list of the currently managed Windows
        private readonly Dictionary<ViewModel, Window> _viewModelMap; //Maps a ViewModel instance to its managed Window
        private readonly IWindowTypeResolver _windowTypeResolver;

        public IWindowInitializer WindowInitializer { get; set; }

        public WindowManagerService()
        {
            _windowMap = new Dictionary<Type, Type>();
            _openWindows = new List<Window>();
            _viewModelMap = new Dictionary<ViewModel, Window>();
            _windowTypeResolver = new DefaultWindowTypeResolver();

            WindowInitializer = new DefaultInitializer();
        }

        public WindowManagerService(IWindowTypeResolver windowTypeResolver) : this()
        {
            _windowTypeResolver = windowTypeResolver;
        }

        public IWindowManagerService RegisterDefaultWindow<TViewModelType, TWindowType>()
        {
            _windowMap.Add(typeof(TViewModelType), typeof(TWindowType));
            return this;
        }

        public Window DisplayWindowFor(ViewModel viewModel)
        {
            var viewType = ResolveWindowFor(viewModel);
            if (viewType != null)
            {
                return DisplayWindowFor(viewModel, viewType);
            }

            throw new InvalidOperationException($"{viewModel.GetType()} does not have a default view.");
        }

        public Window DisplayWindowFor(ViewModel viewModel, Type windowType)
        {
            CheckForAttachedWindow(viewModel);

            Window view = CreateAndBindWindowFor(windowType, viewModel);
            view.Show();
            return view;
        }

        public Window DisplayChildWindowFor(ViewModel childViewModel, ViewModel ownerViewModel)
        {
            Window view = DisplayWindowFor(childViewModel);
            view.Owner = _viewModelMap[ownerViewModel];
            return view;
        }

        public Window DisplayChildWindowFor(ViewModel childViewModel)
        {
            Window view = DisplayWindowFor(childViewModel);
            view.Owner = _openWindows.LastOrDefault();
            return view;
        }

        public bool DisplayDialogFor(ViewModel viewModel)
        {
            CheckForAttachedWindow(viewModel);
            var viewType = ResolveWindowFor(viewModel);

            if (viewType != null)
            {
                Window view = CreateAndBindWindowFor(viewType, viewModel, _openWindows.LastOrDefault());
                return view.ShowDialog().GetValueOrDefault(false);
            }
            throw new InvalidOperationException($"Unable to resolve a view for {viewModel.GetType()}.");
        }

        /// <summary>
        /// Returns the Window Type for a specified ViewModel
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        /// <remarks>First checks for explicitly registers Windows, then uses the IViewResolver.</remarks>
        private Type ResolveWindowFor(ViewModel viewModel)
        {
            Type windowType = null;
            if (!_windowMap.TryGetValue(viewModel.GetType(), out windowType))
            {
                windowType = _windowTypeResolver?.ResolveWindowType(viewModel);
            }
            return windowType;
        }

        private Window CreateAndBindWindowFor(Type windowType, ViewModel viewModel)
        {
            return CreateAndBindWindowFor(windowType, viewModel, null);
        }

        /// <summary>
        /// Instantiates the Window for the specified type, and attaches it to the Service.
        /// </summary>
        /// <param name="windowType">The Type of Window to create</param>
        /// <param name="viewModel">the ViewModel instance to bind to</param>
        /// <param name="owner">The optional Window to mark as the owner of the new window</param>
        /// <returns></returns>
        private Window CreateAndBindWindowFor(Type windowType, ViewModel viewModel, Window owner)
        {
            Window window = (Window)Activator.CreateInstance(windowType);
            WindowInitializer?.InitializeWindow(window);
            window.Owner = owner;
            window.DataContext = viewModel;
            window.Closed += Window_Closed;
            window.Closing += Window_Closing;
            _openWindows.Add(window);
            _viewModelMap.Add(viewModel, window);
            return window;
        }

        /// <summary>
        /// Sets the DialogResult on the Window attached to the spcified ViewModel.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="dialogResult"></param>
        public void SetDialogResult(ViewModel viewModel, bool dialogResult)
        {
            Window view = null;
            if (_viewModelMap.TryGetValue(viewModel, out view))
            {
                view.DialogResult = dialogResult;
            }
            else
            {
                throw new InvalidOperationException("There is no managed View for the specified viewModel");
            }
        }

        /// <summary>
        /// Gives the attached view model a chance to cancel the window close if it implements IWindowClosingQueryable.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, EventArgs e)
        {
            ViewModel vm = (ViewModel)((Window)sender).DataContext;
            IWindowClosingQueryable q = vm as IWindowClosingQueryable;
            if (q != null)
            {
                CancelEventArgs windowArgs = e as CancelEventArgs;
                q.WindowClosing(windowArgs);
            }
        }

        /// <summary>
        /// Unregisters a closed window with the Service
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            var window = (Window)sender;
            var viewModel = (ViewModel)window.DataContext;
            window.Closed -= Window_Closed;
            window.Closing -= Window_Closing;
            _openWindows.Remove(window);
            _viewModelMap.Remove(viewModel);

            if (_openWindows.Count == 0)
            {
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// Closes the Window that is attached to the specified viewModel
        /// </summary>
        /// <param name="viewModel"></param>
        public void Close(ViewModel viewModel)
        {
            var view = _viewModelMap[viewModel];
            view.Close();
        }

        /// <summary>
        /// Closes all managed Windows
        /// </summary>
        public void CloseAll()
        {
            var windows = new List<Window>(_openWindows);
            foreach (Window view in windows)
            {
                view.Close();
            }
        }

        /// <summary>
        /// Displays a simple popup window
        /// </summary>
        /// <param name="message"></param>
        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }

        /// <summary>
        /// Displays a popup message and waits for a result
        /// </summary>
        /// <param name="ownerVm">The ViewModel instance connected to a Window that will own the popup</param>
        /// <param name="message">The text of the popup</param>
        /// <param name="caption">The title of the popup</param>
        /// <param name="button">The buttons that will be displayed</param>
        /// <param name="image">The icon that will be displayed in the popup</param>
        /// <param name="defaultResult">The default button of the popup</param>
        /// <returns></returns>
        public MessageBoxResult GetMessageBoxResult(ViewModel ownerVm, string message, string caption, MessageBoxButton button, MessageBoxImage image, MessageBoxResult defaultResult)
        {
            MessageBoxResult result = MessageBox.Show(_viewModelMap[ownerVm], message, caption, button, image, defaultResult);
            return result;
        }

        /// <summary>
        /// Throws an exception if the specified ViewModel is already attached to a Window
        /// </summary>
        /// <param name="viewModel"></param>
        private void CheckForAttachedWindow(ViewModel viewModel)
        {
            if (_viewModelMap.ContainsKey(viewModel))
            {
                throw new InvalidOperationException("The specified viewModel instance is already attached to a Window");
            }
        }
    }
}
