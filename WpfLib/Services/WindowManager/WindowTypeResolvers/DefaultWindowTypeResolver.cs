﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace AgentOctal.WpfLib.Services.WindowManager.WindowTypeResolvers
{
    /// <summary>
    /// Tries to resolve the view from he view model name.
    /// If the View Model name ends with "VM" it will try to resolve 
    /// a View with the same name without the VM. 
    /// ie, "MainWindowVm" will resolve to "MainWindow".
    /// </summary>
    public class DefaultWindowTypeResolver : IWindowTypeResolver
    {
        private readonly Dictionary<Type, Type> _typeCache = new Dictionary<Type, Type>();

        public Type ResolveWindowType(ViewModel viewModel)
        {
            Type viewModelType = viewModel.GetType();

            //Check for a cached association
            Type windowType = null;
            if (_typeCache.TryGetValue(viewModelType, out windowType))
            {
                return windowType;
            }

            //begin searching for a Type with the same name as the ViewModel Type without the "Vm" suffix.
            string viewModelName = viewModelType.Name;
            if (viewModelName.EndsWith("Vm", StringComparison.OrdinalIgnoreCase))
            {
                string potentialViewName = viewModelName.Substring(0, viewModelName.Length - 2);
                Assembly asm = viewModelType.Assembly;
                foreach (Type type in asm.GetTypes().Where(t=> t.IsSubclassOf(typeof(Window))))
                {
                    if (type.Name.Equals(potentialViewName, StringComparison.OrdinalIgnoreCase))
                    {
                        _typeCache.Add(viewModelType, type); //cache the result so we don't have to search again next time.
                        return type;
                    }
                }
            }
            return null;
        }
    }
}
