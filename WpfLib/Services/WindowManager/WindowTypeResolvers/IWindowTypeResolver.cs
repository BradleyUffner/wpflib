﻿using System;

namespace AgentOctal.WpfLib.Services.WindowManager.WindowTypeResolvers
{
    /// <summary>
    /// Used to locate a View for a particular ViewModel based on conventions.
    /// </summary>
    public interface IWindowTypeResolver
    {

        Type ResolveWindowType(ViewModel viewModel);
    }
}
