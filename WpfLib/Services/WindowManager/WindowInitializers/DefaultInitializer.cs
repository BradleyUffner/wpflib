﻿using System.Windows;
using System.Windows.Media;

namespace AgentOctal.WpfLib.Services.WindowManager.WindowInitializers
{

    /// <summary>
    /// Initializes windows by setting TextFormattingMode to Display (Makes the windows look nicer).
    /// </summary>
    public class DefaultInitializer : IWindowInitializer
    {
        public virtual void InitializeWindow(Window window)
        {
            window.SetValue(TextOptions.TextFormattingModeProperty, TextFormattingMode.Display);
            window.SnapsToDevicePixels = true;
        }
    }
}
