﻿using System.Windows;

namespace AgentOctal.WpfLib.Services.WindowManager.WindowInitializers
{

    /// <summary>
    /// An interface used to initialize new windows with default values,
    /// or run other functions at window creation.
    /// </summary>
    public interface IWindowInitializer
    {
        /// <summary>
        /// Invoked after a new window is instantiated.
        /// </summary>
        /// <param name="window">The newly created window.</param>
        void InitializeWindow(Window window);
    }
}
