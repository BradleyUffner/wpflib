﻿using System.Windows;

namespace AgentOctal.WpfLib.Services.Dialog
{
    public class DialogService : IDialogService
    {
        public void Show(string message)
        {
            MessageBox.Show(message);
        }
    }
}
