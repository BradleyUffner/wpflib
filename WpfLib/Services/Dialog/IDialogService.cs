﻿namespace AgentOctal.WpfLib.Services.Dialog
{
    public interface IDialogService
    {
        void Show(string message);
    }
}
