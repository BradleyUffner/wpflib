﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentOctal.WpfLib
{
    public interface IWindowClosingQueryable
    {
        void WindowClosing(CancelEventArgs e);
    }
}
