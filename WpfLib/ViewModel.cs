﻿using System.ComponentModel;
using System.Windows;

namespace AgentOctal.WpfLib
{
    public class ViewModel : ObservableObject
    {
        public static bool IsInDesignMode => DesignerProperties.GetIsInDesignMode(new DependencyObject());
    }
}
