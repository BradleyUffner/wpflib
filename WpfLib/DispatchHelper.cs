﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace AgentOctal.WpfLib
{
    public static class DispatchHelper
    {
        public static void Invoke(Action action)
        {
            var d = Application.Current.Dispatcher;
            if (d == null || d.CheckAccess())
            {
                action();
            }
            else
            {
                d.Invoke(action, DispatcherPriority.Normal);
            }
        }

        public static void BeginInvoke(Action action)
        {
            BeginInvoke(action, DispatcherPriority.Normal);
        }

        public static void BeginInvoke(Action action, DispatcherPriority priority)
        {
            var d = Application.Current.Dispatcher;
            if (d == null || d.CheckAccess())
            {
                action();
            }
            else
            {
                d.BeginInvoke(action, DispatcherPriority.Normal);
            }
        }

        /// <summary>
        /// Checks if the current call is being made from the dispatcher's thread.
        /// </summary>
        /// <returns>True if Dispatch is required to move to the dispatcher's thread</returns>
        public static bool IsDispatchRequired => !(bool)(Application.Current.Dispatcher?.CheckAccess()).GetValueOrDefault(true);
    }
}
