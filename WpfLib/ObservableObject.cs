﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using AgentOctal.WpfLib.Services;
using AgentOctal.WpfLib.Services.WindowManager;

namespace AgentOctal.WpfLib
{
    public class ObservableObject : INotifyPropertyChanged
    {

        public static bool AutoDispatchPropertyChanged { get; set; } = false;

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (AutoDispatchPropertyChanged && DispatchHelper.IsDispatchRequired)
            {
                DispatchHelper.BeginInvoke(() => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName)));
            }
            else
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Returns the currently registered IWindowManagerService.
        /// </summary>
        protected IWindowManagerService WindowManager => ServiceManager.GetService<IWindowManagerService>();

        /// <summary>
        /// Invokes the Action on the UI thread if needed.
        /// </summary>
        /// <param name="action">The Action to invoke</param>
        /// <param name="useBeginInvoke">Uses Dispatcher.BeginInvoke if true, otherwise uses Dispatcher.Invoke.</param>
        public void InvokeIfRequired(Action action, bool useBeginInvoke)
        {
            var dispatcher = Application.Current.Dispatcher;
            if (!dispatcher.CheckAccess())
            {
                if (useBeginInvoke)
                {
                    dispatcher.BeginInvoke(action);
                }
                else
                {
                    dispatcher.Invoke(action);
                }
            }
            else
            {
                action.Invoke();
            }
        }

        /// <summary>
        /// Invokes the Action on the UI thread if needed.
        /// </summary>
        /// <param name="action">The Action to invoke.</param>
        public void InvokeIfRequired(Action action)
        {
            InvokeIfRequired(action, false);
        }

        /// <summary>
        /// Updates the value of a property backing store and autmatically raises the PropertyChanged event if required.
        /// </summary>
        /// <typeparam name="T">The Type of the property being updated.</typeparam>
        /// <param name="backingField">The backing field for the property.</param>
        /// <param name="newValue">The new value for the property, usually 'value'.</param>
        /// <param name="propertyName">The name of the property being modified.
        /// If omitted, it will be set by the compiler to the name of the 
        /// currently executing method (usually the property).</param>
        /// <returns>Returns true if the property was a new value, false if the new value was the same as the old value.</returns>
        /// <remarks>The PropertyChanged event is only raised if the new value is different than the old value.</remarks>
        protected bool SetValue<T>(ref T backingField, T newValue, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(backingField, newValue))
            {
                return false;
            }
            backingField = newValue;
            RaisePropertyChanged(propertyName);
            return true;
        }
    }
}
