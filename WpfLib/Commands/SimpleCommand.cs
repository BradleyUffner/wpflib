﻿using System;
using System.Windows.Input;

namespace AgentOctal.WpfLib.Commands
{
    public class SimpleCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Action<object> _execute;
        private readonly Func<bool> _canExecute;

        public SimpleCommand(Action<object> execute) : this(execute, null) { }

        public SimpleCommand(Action<object> execute, Func<bool> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object param)
        {
            return _canExecute == null || _canExecute.Invoke();
        }

        public void Execute(object param)
        {
            _execute.Invoke(param);
        }

        protected void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        #region Common Commands
        private static SimpleCommand _notImplementedCommand;
        public static ICommand NotImplementedCommand
        {
            get
            {
                return _notImplementedCommand ??
                       (_notImplementedCommand = new SimpleCommand(o =>
                       {
                           throw new NotImplementedException();
                       }));
            }
        }
        #endregion
    }
}
