﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace AgentOctal.WpfLib.Commands
{
    public class AutoCommand<TViewModel> : SimpleCommand where TViewModel : INotifyPropertyChanged
    {

        private readonly List<string> _propertiesToWatch;

        public AutoCommand(TViewModel viewModelInstance,
                           Action<object> execute,
                           Func<bool> canExecute,
                           Expression<Func<TViewModel, object>> propertiesToWatch) : base(execute, canExecute)
        {
            _propertiesToWatch = RegisterPropertiesWatcher(propertiesToWatch);
            viewModelInstance.PropertyChanged += PropertyChangedHandler;
        }

        private void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            if (_propertiesToWatch.Contains(e.PropertyName))
            {
                OnCanExecuteChanged();
            }
        }

        protected List<string> RegisterPropertiesWatcher(Expression<Func<TViewModel, object>> selector)
        {
            var properties = new List<string>();

            var lambda = (LambdaExpression)selector;

            if (lambda.Body is MemberExpression)
            {
                var memberExpression = (MemberExpression)(lambda.Body);
                properties.Add(memberExpression.Member.Name);
            }
            else if (lambda.Body is UnaryExpression)
            {
                var unaryExpression = (UnaryExpression)(lambda.Body);

                properties.Add(((MemberExpression)(unaryExpression.Operand)).Member.Name);
            }
            else if (lambda.Body.NodeType == ExpressionType.New)
            {
                var newExp = (NewExpression)lambda.Body;
                foreach (var exp in newExp.Arguments.Select(argument => argument as MemberExpression)) {
                    if (exp != null)
                    {
                        var mExp = exp;
                        properties.Add(mExp.Member.Name);
                    }
                    else
                    {
                        throw new SyntaxErrorException("Syntax Error, selector has to be an expression " +
                                                       "that returns a new object containing a list of " +
                                                       "properties, e.g.: s => new { s.Property1, s.Property2 }");
                    }
                }
            }
            else
            {
                throw new SyntaxErrorException("Syntax Error, selector has to be an expression that " +
                                               "returns a new object containing a list of properties, " +
                                               "e.g.: s => new { s.Property1, s.Property2 }");
            }

            return properties;
        }
    }
}
