﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace AgentOctal.WpfLib.Commands
{
    public class MvvmCommandBinding : Freezable
    {
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command",
                                        typeof(ICommand),
                                        typeof(MvvmCommandBinding),
                                        new PropertyMetadata(default(ICommand)));

        public ICommand Command
        {
            get { return (ICommand) GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }


        public static readonly DependencyProperty RoutedCommandProperty =
            DependencyProperty.Register("RoutedCommand",
                                        typeof(Type),
                                        typeof(MvvmCommandBinding),
                                        new PropertyMetadata(default(Type)));

        public Type RoutedCommand
        {
            get { return (Type) GetValue(RoutedCommandProperty); }
            set { SetValue(RoutedCommandProperty, value); }
        }

        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }
    }
}