﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace AgentOctal.WpfLib.Commands
{
    public class MvvmRoutedCommand : ICommand
    {
        public virtual bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object invokingElement)
        {
            var element = invokingElement as DependencyObject;

            while (element != null)
            {
                var commandBindings = (FreezableCollection<MvvmCommandBinding>) element.GetValue(MvvmCommandBindings.CommandsProperty);
                if (commandBindings != null)
                {
                    foreach (var binding in commandBindings.Where(b => b.RoutedCommand == this.GetType()))
                    {
                        var command = binding.Command;
                        if (command.CanExecute(null))
                        {
                            command.Execute(null);
                        }
                    }
                }
                element = VisualTreeHelper.GetParent(element);
            }
        }

        public event EventHandler CanExecuteChanged;

        public virtual void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this,EventArgs.Empty);
        }
    }
}
