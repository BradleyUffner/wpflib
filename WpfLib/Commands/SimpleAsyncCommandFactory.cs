﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace AgentOctal.WpfLib.Commands
{
    /// <summary>
    /// A factory to assist in creating IAsyncCommand
    /// </summary>
    public static class SimpleAsyncCommandFactory
    {
        /// <summary>
        /// Creates an IAsyncCommand that returns no value and can not be canceled.
        /// </summary>
        /// <param name="command">The method to invoke when the command is executed.</param>
        /// <returns></returns>
        public static SimpleAsyncCommand<object> Create(Func<Task> command)
        {
            return new SimpleAsyncCommand<object>(async _ => { await command(); return null; });
        }

        /// <summary>
        /// Creates an IAsyncCommand that returns a TResult and can not be canceled.
        /// </summary>
        /// <typeparam name="TResult">The type of result</typeparam>
        /// <param name="command">The method to invoke when the command is executed.</param>
        /// <returns></returns>
        public static SimpleAsyncCommand<TResult> Create<TResult>(Func<Task<TResult>> command)
        {
            return new SimpleAsyncCommand<TResult>(_ => command());
        }

        /// <summary>
        /// Creates an IAsyncCommand that returns no value and can not be canceled.
        /// </summary>
        /// <param name="command">The method to invoke when the command is executed.</param>
        /// <returns></returns>
        public static SimpleAsyncCommand<object> Create(Func<CancellationToken, Task> command)
        {
            return new SimpleAsyncCommand<object>(async token => { await command(token); return null; });
        }

        /// <summary>
        /// Creates an IAsyncCommand that returns no value and can be canceled.
        /// </summary>
        /// <typeparam name="TResult">The type of result</typeparam>
        /// <param name="command">The method to invoke when the command is executed.</param>
        /// <returns></returns>
        public static SimpleAsyncCommand<TResult> Create<TResult>(Func<CancellationToken, Task<TResult>> command)
        {
            return new SimpleAsyncCommand<TResult>(command);
        }
    }
}