﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace AgentOctal.WpfLib.Commands
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
    }
}
