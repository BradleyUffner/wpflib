﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace AgentOctal.WpfLib.Commands
{
    public class MvvmCommandBindings : FrameworkElement
    {

        private static readonly FreezableCollection<MvvmCommandBinding> _defaulCommandCollection = new FreezableCollection<MvvmCommandBinding>();

        public static readonly DependencyProperty CommandsProperty =
            DependencyProperty.RegisterAttached("CommandsInternal",
                                                        typeof(FreezableCollection<MvvmCommandBinding>),
                                                        typeof(FrameworkElement),
                                                        new FrameworkPropertyMetadata(_defaulCommandCollection));


        public static void SetCommands(UIElement element, FreezableCollection<MvvmCommandBinding> value)
        {
            element.SetValue(CommandsProperty, value);
        }

        public static FreezableCollection<MvvmCommandBinding> GetCommands(UIElement element)
        {

            var commands = (FreezableCollection<MvvmCommandBinding>)element.GetValue(CommandsProperty);
            if (ReferenceEquals(commands, _defaulCommandCollection))
            {
                commands = new FreezableCollection<MvvmCommandBinding>();
                SetCommands(element, commands);
            }
            return commands;
        }
    }
}