﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace AgentOctal.WpfLib
{
    public class ThisExtension : MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return ((IProvideValueTarget)serviceProvider.GetService(typeof(IProvideValueTarget)))?.TargetObject;
        }
    }
}
