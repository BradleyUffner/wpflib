﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Markup;

namespace AgentOctal.WpfLib
{
    //Based on http://www.jonathanantoine.com/2011/09/23/wpf-4-5s-markupextension-invoke-a-method-on-the-viewmodel-datacontext-when-an-event-is-raised/
    [MarkupExtensionReturnType(typeof(Delegate))]
    public class VmCall : MarkupExtension
    {
        public string ActionName { get; set; }
        public bool PassEventArgs { get; set; }

        private MethodInfo _proxyHandlerInfo;

        public VmCall()
        {
            PassEventArgs = false;
        }

        public VmCall(string actionName)
        {
            ActionName = actionName;
            PassEventArgs = false;
        }

        public VmCall(string actionName, bool passEventArgs)
        {
            ActionName = actionName;
            PassEventArgs = passEventArgs;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (!(serviceProvider
                .GetService(typeof(IProvideValueTarget)) is IProvideValueTarget targetProvider))
                throw new InvalidOperationException($@"The {nameof(VmCall)} markup extension can't retrieve the IProvideValueTarget service.");

            if (!(targetProvider.TargetObject is FrameworkElement target))
                throw new InvalidOperationException($@"The {nameof(VmCall)} markup extension can only be used on a FrameworkElement.");

            Type delegateType = null;
            switch (targetProvider.TargetProperty)
            {
                case MethodInfo targetEventAddMethod:
                {
                    ParameterInfo[] pars = targetEventAddMethod.GetParameters();
                    delegateType = pars[1].ParameterType;
                    break;
                }
                case EventInfo targetEventInfo:
                {
                    delegateType = targetEventInfo.EventHandlerType;
                    break;
                }
                default:
                {
                    throw new InvalidOperationException($@"The {nameof(VmCall)} markup extension can only be used on a event.");
                }
            }

            _proxyHandlerInfo = _proxyHandlerInfo ?? this.GetType()
                                                         .GetMethod("ProxyHandler",
                                                                    BindingFlags.NonPublic |
                                                                    BindingFlags.Instance);

            var del = Delegate.CreateDelegate(delegateType, this, _proxyHandlerInfo);
            return del;
        }

        void ProxyHandler(object sender, EventArgs e)
        {
            if (!(sender is FrameworkElement target)) return;
            var dataContext = target.DataContext;
            if (dataContext == null) return;

            MethodInfo methodInfo;
            if (PassEventArgs)
            {
                methodInfo = dataContext.GetType()
                                        .GetMethod(ActionName,
                                                   BindingFlags.Public |
                                                   BindingFlags.Instance,
                                                   null,
                                                   new [] {typeof(EventArgs)},
                                                   null);
                methodInfo?.Invoke(dataContext, new object[] {e});
            }
            else
            {
                methodInfo = dataContext.GetType()
                                        .GetMethod(ActionName,
                                                   BindingFlags.Public |
                                                   BindingFlags.Instance);
                methodInfo?.Invoke(dataContext, null);
            }
        }
    }
}