﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Serialization;

namespace AgentOctal.WpfLib
{
    public class ObservableCollection<T> : System.Collections.ObjectModel.ObservableCollection<T>
    {
        public bool SuppressNotification { get; set; } = false;
        public RangeSupport RangeSupport { get; set; } = RangeSupport.None;

        public void AddRange(IEnumerable<T> collection)
        {
            if (!(collection is IList newItems))
            {
                newItems = new List<T>(collection);
            }
            var startIndex = Items.Count;
            foreach (T i in newItems)
            {
                Items.Add(i);
            }
            var args = RangeSupport == RangeSupport.Full 
                ? new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, newItems, startIndex) 
                : new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            
            OnCollectionChanged(args);
        }
        public void RemoveRange(IEnumerable<T> collection)
        {
            foreach (T i in collection)
            {
                Items.Remove(i);
            }
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void ReplaceRange(IEnumerable<T> collection)
        {
            var old = Items.ToList();
            Items.Clear();
            foreach (T i in collection)
            {
                Items.Add(i);
            }
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (SuppressNotification)
            {
                return;
            }
            base.OnCollectionChanged(e);
        }

        public void ResetNotification()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }
}
