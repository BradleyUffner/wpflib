﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentOctal.WpfLib
{
    public class CompositeObservableCollectionView<T> :ObservableCollection<T>
    {
        protected CompositeObservableCollectionView()
        {
            _collections = new List<CollectionData>();
            _collectionLookup = new Dictionary<ObservableCollection<T>, CollectionData>();
        }

        public CompositeObservableCollectionView(IEnumerable<ObservableCollection<T>> collections):this()
        {
            _collections.AddRange(collections.Select(c=>new CollectionData(c)));
            foreach (var collectionData in _collections)
            {
                collectionData.Collection.CollectionChanged += MemberCollection_CollectionChanged;
            }

            Reindex();
        }

        private void MemberCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var source = _collectionLookup[(ObservableCollection<T>) sender];
            var newStartingIndex = source.StartIndex + e.NewStartingIndex;
            var oldStartingIndex = source.StartIndex + e.OldStartingIndex;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    break;
                case NotifyCollectionChangedAction.Remove:
                    break;
                case NotifyCollectionChangedAction.Replace:
                    break;
                case NotifyCollectionChangedAction.Move:
                    break;
                case NotifyCollectionChangedAction.Reset:
                    Reindex();
                    OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        private readonly List<CollectionData> _collections;
        private readonly Dictionary<ObservableCollection<T>,CollectionData> _collectionLookup;

        private class CollectionData
        {
            public CollectionData(ObservableCollection<T> collection)
            {
                Collection = collection;
            }

            public ObservableCollection<T> Collection { get; }
            public int StartIndex { get; set; }
        }

        private void Reindex()
        {
            int currentIndex = 0;
            foreach (var collectionData in _collections)
            {
                collectionData.StartIndex = currentIndex;
                currentIndex += collectionData.Collection.Count;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {

            foreach (var collectionData in _collections)
            {
                foreach (var item in collectionData.Collection)
                {
                    yield return item;
                }
            }
        }
    }
}
