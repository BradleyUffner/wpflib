﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace AgentOctal.WpfLib.Converters
{

    public class MappingConverter:IValueConverter
    {
        public List<Mapping> Mappings { get; set; }

        public MappingConverter()
        {
            Mappings = new List<Mapping>();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = Mappings.FirstOrDefault(m => m.From == value.ToString());
            return result?.To;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class Mapping
    {
        public string From { get; set; }
        public object To { get; set; }
    }
}
