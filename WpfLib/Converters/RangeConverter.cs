﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace AgentOctal.WpfLib.Converters
{
    /// <summary>
    /// Converts a ranges of input values in to single output values, ie. 1-10 = red, 11-20 = Green, etc.
    /// </summary>
    public class RangeConverter : IValueConverter
    {
        public List<Range> Ranges { get; set; }

        public RangeConverter()
        {
            Ranges = new List<Range>();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = (IComparable)value;
            var range = Ranges.FirstOrDefault(r => r.Value.CompareTo(v) < 0);
            return range?.Result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A range of input values and their corisponding output value.
    /// </summary>
    public class Range
    {
        public IComparable Value { get; set; }
        public object Result { get; set; }
    }
}
