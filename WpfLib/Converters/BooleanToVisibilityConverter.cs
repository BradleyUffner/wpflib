﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace AgentOctal.WpfLib.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public Visibility TrueVisibilityType { get; set; } = Visibility.Visible;
        public Visibility FalseVisibilityType { get; set; } = Visibility.Collapsed;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? TrueVisibilityType : FalseVisibilityType;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
