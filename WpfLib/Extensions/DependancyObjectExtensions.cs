﻿using System.Windows;
using System.Windows.Media;

// ReSharper disable once CheckNamespace
namespace GumpStudio.WpfLib
{
    public static class DependancyObjectExtensions
    {
        public static T FindParentOf<T>(this DependencyObject o) where T : DependencyObject
        {
            DependencyObject parentObject = VisualTreeHelper.GetParent(o);
            if (parentObject == null)
            {
                return null;
            }

            var parent = parentObject as T;
            return parent ?? FindParentOf<T>(parentObject);
        }

        public static T GetChildOf<T>(this DependencyObject o) where T : DependencyObject
        {
            return GetChildOf<T>(o, false);
        }

        public static T GetChildOf<T>(this DependencyObject o, bool includeSelf) where T : DependencyObject
        {
            if (o == null)
            {
                return null;
            }

            if (includeSelf && o is T)
            {
                return (T) o;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = (child as T) ?? GetChildOf<T>(child);
                if (result != null) return result;
            }
            return null;
        }

    }
}
